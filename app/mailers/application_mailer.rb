class ApplicationMailer < ActionMailer::Base
  default from: 'contact@bikeberlin.com'
  layout 'mailer'

  def thank_you
  @name = params[:name]
  @email = params[:email]
  @message = params[:message]
  ActionMailer::Base.mail(from: @email,
      to: 'allicn4189@gmail.com',
      subject: "A new contact form message from #{@name}",
      body: @message).deliver_now
  end
end
