class UserMailer < ApplicationMailer
  default from: "contact@bikeberlin.com"

  def contact_form(email, name, message)
  @message = message
    mail(from: email,
         to: 'allicn4189@gmail.com',
         subject: "A new contact form message from #{name}")
  end
end
